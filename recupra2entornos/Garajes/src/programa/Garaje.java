package programa;

import java.time.LocalDate;
import java.util.Scanner;

public class Garaje {
	
	static String[][] garaje = new String[10][3];
	
	public static void main(String[] args) {
		
		inicializarGaraje();
				
		Scanner input = new Scanner(System.in);
		int opcion;
		
		do{
			mostrarMenu();
			opcion = input.nextInt();
			
			switch(opcion){
				case 1:
					mostrarPlazas();
					break;
				case 2: 
					mostrarClientes();
					break;
				case 3:
					alquilarPlaza(input);
					break;
				case 4:
					liberarPlaza(input);
					break;
					
				default:
					System.out.println("Opcion no contemplada");
			}
		}while(opcion != 5);
		
		input.close();
	}

	private static void liberarPlaza(Scanner input) {
		int plazaEliminar;
		System.out.println("Introduce numero de plaza [1-10]");
		plazaEliminar = input.nextInt();
		if(estaVacia(plazaEliminar)){
			System.out.println("La plaza ya esta vacia");
		}else{
			garaje[plazaEliminar][0] = "vacio";
		}
		
	}

	private static void mostrarClientes() {
		System.out.println("Clientes:");
		for (int i = 0; i < garaje.length; i++) {
			if(!garaje[i][0].equals("vacio")){
				System.out.println("Plaza: " + (i+1));
				System.out.println("Nombre: " + garaje[i][0]);
				System.out.println("Matricula: " + garaje[i][1]);
				System.out.println("Fecha Alquiler: " + garaje[i][2]);
			}
		}	
	}

	private static void mostrarPlazas() {
		System.out.println("Plazas:");
		for (int i = 0; i < garaje.length; i++) {
			
			if(garaje[i][0].equals("vacio")){
				System.out.println((i+1)+": Libre");
			}else{
				System.out.println((i+1)+": Ocupada");
			}
		}
	}

	private static void alquilarPlaza(Scanner input) {
		int plazaAlquilar;
		System.out.println("Introduce numero de plaza [1-10]");
		plazaAlquilar = input.nextInt();
		input.nextLine();
		if(estaVacia(plazaAlquilar)){
			System.out.println("Introduce nombre y apellidos");
			garaje[plazaAlquilar-1][0] = input.nextLine();
			System.out.println("Introduce matricula");
			garaje[plazaAlquilar-1][1] = input.nextLine();
			garaje[plazaAlquilar-1][2] = LocalDate.now().toString();
		}else{
			System.out.println("Plaza no disponible");
		}
	}

	private static void mostrarMenu() {
		System.out.println("\n*********************");
		System.out.println("1: Mostrar plazas");
		System.out.println("2: Mostrar clientes");
		System.out.println("3: Alquilar plaza");
		System.out.println("4: Eliminar alquiler");
		System.out.println("5: Salir");
		System.out.println("Selecciona una opcion");
		System.out.println("\n*********************\n\n");
	}


	private static void inicializarGaraje(){
		for (int i = 0; i < garaje.length; i++) {
			garaje[i][0] = "vacio";
		}
	}
	

	private static boolean estaVacia(int numPlaza){
		if(numPlaza >= 1 && numPlaza <= 10 && garaje[numPlaza-1][0].equals("vacio")){
			return true;
		}else{
			return false;
		}
	}
	
	
	
}
